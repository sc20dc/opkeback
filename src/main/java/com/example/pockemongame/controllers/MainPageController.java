package com.example.pockemongame.controllers;

import com.example.pockemongame.entities.Pokemon;
import com.example.pockemongame.services.PokemonService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import java.util.List;

//Endpoints that receives and responses the result of services
@RestController
@CrossOrigin(origins = "http://localhost:3000/")
public class MainPageController {

    private final PokemonService pokemonService;

    @Autowired
    public MainPageController(PokemonService pokemonService) {
        this.pokemonService = pokemonService;
    }



    @GetMapping("/getPokemonByname")
    public Pokemon getPokemonByName(@RequestParam ("name") String name ){
        return pokemonService.getPokemonByName(name);
    }

    @GetMapping("/getAllPokemons")
    public List<Pokemon> getAllPokemons(){
        return pokemonService.getAllPokemons();
    }

    @GetMapping("/getAWinner")
    public String getAWinner(@RequestParam ("my") String myName, @RequestParam ("enemy") String enemyName){
        return pokemonService.countWinner(myName, enemyName);
    }

    @PatchMapping("/updateAPokeon")
    void updateAPokemon(@RequestParam ("id") int id, @RequestParam ("name") String name, @RequestParam ("hp") int hp,
                        @RequestParam ("attack") int attack, @RequestParam ("defence") int defence,
                        @RequestParam ("speed") int speed){
        pokemonService.updateAPokemon(id, name, hp, attack, defence, speed);
    }
    @DeleteMapping("/deleteAPokemon")
    void deleteAPokemonByName(@RequestParam ("name") String name){
        pokemonService.deleteAPokemonByName(name);
    }

    @PatchMapping("/insertAPokemon")
    void insertAPokemon(@RequestParam ("name") String name, @RequestParam ("hp") int hp,
                        @RequestParam ("attack") int attack, @RequestParam ("defence") int defence,
                        @RequestParam ("speed") int speed){
        pokemonService.insertAPokemon(name, hp, attack, defence, speed);
    }

}
