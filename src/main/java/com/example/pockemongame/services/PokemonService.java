package com.example.pockemongame.services;

import com.example.pockemongame.entities.Pokemon;
import com.example.pockemongame.repositories.PokemonRepositoryImplementation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import java.util.List;
import java.util.Random;
import java.util.concurrent.ThreadLocalRandom;

// The service layer is created to implement the main logic of the requests
@Service
@Transactional(readOnly = true)
public class PokemonService {

    private PokemonRepositoryImplementation pokemonRepositoryImplementation;

    @Autowired
    public PokemonService(PokemonRepositoryImplementation pokemonRepositoryImplementation) {
        this.pokemonRepositoryImplementation = pokemonRepositoryImplementation;
    }

    public Pokemon getPokemonByName(String name) {
        return pokemonRepositoryImplementation.getPokemonByName(name);
    }

    public List<Pokemon> getAllPokemons() {
        return pokemonRepositoryImplementation.getAllPokemons();
    }
    @Transactional(readOnly = false)
    public void updateAPokemon(int id, String name, int hp, int attack, int defence, int speed) {
        pokemonRepositoryImplementation.updateAPokemon(id, name, hp, attack, defence, speed);
    }
    @Transactional(readOnly = false)
    public void deleteAPokemonByName(String name) {
        pokemonRepositoryImplementation.deleteAPokemonByName(name);
    }
    @Transactional(readOnly = false)
    public void insertAPokemon(String name, int hp, int attack, int defence, int speed) {
        pokemonRepositoryImplementation.insertAPokemon(name, hp, attack, defence, speed);
    }

    // THis function counts Pokemon stats and returns the one which has bigger stats
    public String countWinner(String myPokeName, String enemyPokeName){
        Pokemon myPoke = pokemonRepositoryImplementation.getPokemonByName(myPokeName);
        Pokemon enemyPoke = pokemonRepositoryImplementation.getPokemonByName(enemyPokeName);
        int myPokeTotal = myPoke.getAttack() + myPoke.getDefence() + myPoke.getHP() + myPoke.getSpeed();
        int enemyPokeTotal = enemyPoke.getAttack() + enemyPoke.getDefence() + enemyPoke.getHP() + enemyPoke.getHP();

        if (myPokeTotal > enemyPokeTotal){
            return myPokeName;
        } else if (myPokeTotal == enemyPokeTotal) {
            //random
            Random random = new Random();
            random.setSeed(42);
            int number = ThreadLocalRandom.current().nextInt(1, 2);
            System.out.println(number);

            if (number == 1) { return myPokeName;} else {return enemyPokeName;}

        } else {
            return enemyPokeName;
        }
    }
}
