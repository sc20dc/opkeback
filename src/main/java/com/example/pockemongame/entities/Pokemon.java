package com.example.pockemongame.entities;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.stereotype.Component;

@Component
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class Pokemon {
    private int id;
    private String pokemonName;
    private int HP;
    private int attack;
    private int defence;
    private int speed;

}
