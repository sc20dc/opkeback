package com.example.pockemongame.mappers;

import com.example.pockemongame.entities.Pokemon;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

// This class is created to map response from database and returns the initialized object with set data
public class PokemonMapper implements RowMapper<Pokemon> {

    @Override
    public Pokemon mapRow(ResultSet rs, int rowNum) throws SQLException {
        Pokemon pokemon = new Pokemon();
        pokemon.setId(rs.getInt(1));
        pokemon.setPokemonName(rs.getString(2));
        pokemon.setHP(rs.getInt(3));
        pokemon.setAttack(rs.getInt(4));
        pokemon.setDefence(rs.getInt(5));
        pokemon.setSpeed(rs.getInt(6));

        return pokemon;
    }
}
