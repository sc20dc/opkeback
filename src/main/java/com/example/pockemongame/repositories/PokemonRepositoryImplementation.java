package com.example.pockemongame.repositories;

import com.example.pockemongame.entities.Pokemon;
import com.example.pockemongame.mappers.PokemonMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;
import java.util.List;

// Repository implements SQL calls toData Base
@Repository
public class PokemonRepositoryImplementation implements PokemonRepository{

    private final JdbcTemplate jdbcTemplate;

    @Autowired
    public PokemonRepositoryImplementation(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    @Override
    public Pokemon getPokemonByName(String name) {
        List<Pokemon> pokemon =  jdbcTemplate.query("select Id, PokeName, Hp, Attack, Defence, Speed from  contactdb.pokemon where PokeName = ?", new PokemonMapper(), name);
        if (pokemon.isEmpty()){
            return null;
        }
        return pokemon.get(0);
    }

    @Override
    public List<Pokemon> getAllPokemons() {
        return jdbcTemplate.query("select Id, PokeName, Hp, Attack, Defence, Speed from  contactdb.pokemon", new PokemonMapper());
    }

    public List<Pokemon> getStatsByName(String name){
        return jdbcTemplate.query("select Id, PokeName, Hp, Attack, Defence, Speed from  contactdb.pokemon", new PokemonMapper());
    }

    @Override
    public void updateAPokemon(int id, String name, int hp, int attack, int defence, int speed) {
        jdbcTemplate.update("update contactdb.pokemon set Id = ?, PokeName = ?, Hp = ?, Attack = ?, Defence = ?, Speed = ? where Id = ?", id, name, hp, attack, defence, speed, id);
    }

    @Override
    public void deleteAPokemonByName(String name) {
        jdbcTemplate.update("DELETE FROM contactdb.pokemon WHERE PokeName = ?", name);
    }

    @Override
    public void insertAPokemon(String name, int hp, int attack, int defence, int speed) {
        jdbcTemplate.update("insert into contactdb.pokemon ( PokeName, Hp, Attack, Defence, Speed) VALUES (?,?,?,?)", name, hp, attack, defence, speed);
    }
}
