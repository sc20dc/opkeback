package com.example.pockemongame.repositories;

import com.example.pockemongame.entities.Pokemon;

import java.util.List;


public interface PokemonRepository {

   Pokemon getPokemonByName(String name);
   List<Pokemon> getAllPokemons();
   List<Pokemon> getStatsByName(String name);
   void updateAPokemon(int id, String name, int hp, int attack, int defence, int speed);
   void deleteAPokemonByName(String name);
   void insertAPokemon(String name, int hp, int attack, int defence, int speed);


}
